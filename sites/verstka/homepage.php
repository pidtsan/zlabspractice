<?php

use ZLabs\Asset\AsyncJs;
use ZLabs\Asset\DeferredJs;
use ZLabs\Asset\DeferredStyles;
use ZLabs\Asset\InlineJs;
use ZLabs\Asset\InlineStyles;
use ZLabs\EnvSingleton;

require_once $_SERVER['DOCUMENT_ROOT'] . '/include/auth.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/../../vendor/autoload.php';

$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/local/assets/mustache/')
]);
?>
<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Новый проект: главная страница</title>
    <?php
    // Если фронтенд собран, покажем стили и скрипты "инлайново"
    if (EnvSingleton::getInstance()->isFrontendMode()) {
        echo (new InlineStyles(collect([
            '/local/assets/local/bundle-common/bundle-common.css',
            '/local/assets/local/bundle-homepage/bundle-homepage.css'
        ])))->render();
        echo (new InlineJs(collect([
        ])))->render();
    }
    ?>
</head>
<body class="page">
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/include/header.php');
?>
<main class="index">
    <?php
    echo $mustache->render(
        'index-example',
        include $_SERVER['DOCUMENT_ROOT'] . '/context/index/index-example.php'
    );
    ?>
</main>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/include/footer.php');

// Если фронтенд не собран (работает команда npm run start), покажем стили и скрипты "отложено", чтобы webpack нормально работал
if (!EnvSingleton::getInstance()->isFrontendMode()) {
    echo (new DeferredJs(collect([
    ]), true))->render();
}

// Если фронтенд собран, покажем отложенные css файлы
if (EnvSingleton::getInstance()->isFrontendMode()) {
    echo (new DeferredStyles(collect([
        '/local/assets/local/vue-app/vue-app.css'
    ]), true))->render();
}

// Эти скрипты всегда будут грузится отложено (defer)
echo (new DeferredJs(collect([
    '/local/assets/local/bundle-common/bundle-common.js',
    '/local/assets/local/bundle-homepage/bundle-homepage.js',
    '/local/assets/local/vue-app/vue-app.js'
]), true))->render();

// Эти скрипты всегда будут грузится отложено (async - нужно использовать scriptsReady в js файлах)
echo (new AsyncJs(collect([
]), true))->render();
?>
</body>
</html>