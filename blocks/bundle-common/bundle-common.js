import $ from 'jquery'

window.$ = window.jQuery = window.jquery = $
require('@fancyapps/fancybox')
import '@fancyapps/fancybox/dist/jquery.fancybox.min.css'
import 'nanoscroller/bin/javascripts/jquery.nanoscroller'
import 'nanoscroller/bin/css/nanoscroller.css'
import './bundle-common.sass'
import Blazy from 'blazy'

window.bLazy = new Blazy()
