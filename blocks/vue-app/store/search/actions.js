import { fetch as fetchPolyfill } from 'whatwg-fetch'

export default {
  async search ({ commit }, payload = { q: null, url: '' }) {
    let response

    if (!payload.q) {
      commit('setItems', [])
    } else {
      if (payload.url === '/context/ajax/search.json') {// Если это верстка делаем get запрос
        response = await fetchPolyfill(payload.url)
      } else {// если это битрикс... делаем post запрос
        let formData = new FormData()
        formData.append('ajax_call', 'y')
        formData.append('INPUT_ID', 'search')
        formData.append('l', '2')
        formData.append('q', payload.q)

        response = await fetchPolyfill(
          payload.url,
          {
            method: 'POST',
            body: formData
          }
        )
      }

      if (response.ok) {
        response = await response.json()

        if (response.success) {
          commit('setItems', response.data.results)
          return response.data.results
        }
      }
    }

    return []
  }
}
