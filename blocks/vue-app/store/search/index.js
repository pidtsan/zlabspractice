import getters from './getters'
import mutations from './mutations'
import actions from './actions'

let state = {
  items: []
}

export const search = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
