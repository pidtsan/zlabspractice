import Vue from 'vue'
import Search from './Search'
import { store } from '../../store'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  'ru': require('./lang/ru.json'),
}

const i18n = new VueI18n({
  locale: 'ru',
  messages,
})

export class SearchInstance {
  constructor (params) {
    this.params = Object.assign(
      {
        el: '#search'
      },
      params
    )
    this.target = document.querySelector(this.params.el)

    this.init()
  }

  init () {
    if (this.target) {
      let action = this.target.getAttribute('data-action')
      let url = this.target.getAttribute('data-url')

      new Vue({
        i18n,
        el: this.params.el,
        store,
        render (h) {
          return h(Search, {
            props: {
              action: action,
              url: url
            }
          })
        }
      })
    }
  }
}
