import './index-example.mustache'

// Пример экспорта анонимной функции по умолчанию
export default () => {
  console.info('hello world!')
}

export const helloWorld = () => {
  console.info('hello world!')
}

// Пример экспорта именованного класса
export class NamedClass {
  constructor (params) {
    this.params = $.extend(true, {},
      params)

    this.init()
  }

  init () {
    console.info('hello, World!')
  }
}