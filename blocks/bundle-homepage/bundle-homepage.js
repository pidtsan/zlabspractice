import './bundle-homepage.sass'
import indexExampleModule, { helloWorld } from 'index-example/index-example'
import { NamedClass } from 'index-example/index-example'

$(document).ready(function () {
  indexExampleModule()
  helloWorld()
  new NamedClass()
})
