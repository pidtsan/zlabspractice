module.exports = {
  postcssOptions: {
    plugins: [
      require('autoprefixer')({browsers: 'last 4 version'}),
    ]
  }
};
