'use strict';

const HOST = process.env.HOST || 'http://127.0.0.1';
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const entry = require('./entry');

process.env.NODE_ENV = 'development';

module.exports = {
  mode: 'development',
  entry: entry,
  output: {
    filename: 'local/[name]/[name].js',
    path: __dirname + '/local/assets/',
    publicPath: '/local/assets/'
  },
  watchOptions: {
    aggregateTimeout: 100,
  },
  devtool: "inline-cheap-module-source-map",
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: "local/[name]/[name].css",
      chunkFilename: "[id].css"
    }),
    new BrowserSyncPlugin(
      {
        host: 'localhost',
        port: 3000,
        proxy: 'http://localhost:3100',
        startPath: '/index.php',
        ignore: ['bitrix', 'vendor'],
        open: true,
        files: [
          {
            match: [
              '**/*.php'
            ],
            options: {
              ignored: [
                'bitrix/**/*',
                'node_modules/**/*',
                'vendor/**/*'
              ]
            }
          }
        ]

      },
      {
        reload: false
      }
    ),
    new CopyWebpackPlugin(
      {
        patterns: [
          {
            from: 'blocks/**/*.mustache',
            to: 'mustache/[name].[ext]'
          }
        ]
      }
    ),
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /(node_modules|bower_components|build\/)/,
        loader: 'vue-loader'
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components|build\/)/,
        loader: "babel-loader",
        options: {
          presets: ['@babel/preset-env'],
          plugins: ['transform-object-rest-spread', 'transform-async-to-generator']
        },
      },
      {
        test: /\.sass$/,
        use: [
          'style-loader',
          'vue-style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: require('./postcss.config')
          },
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              implementation: require('node-sass'),
              sassOptions: {
                indentedSyntax: true
              }
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: require('./postcss.config')
          },
        ]
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          name: 'local/fonts/[name].[ext]?[hash]',
          limit: 10000
        }
      },
      {
        test: /\.(woff|woff2)$/,
        loader: "url-loader",
        options: {
          name: "local/fonts/[name].[ext]?[hash]",
          limit: 10000
        }
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader",
        options: {
          name: "local/fonts/[name].[ext]?[hash]",
          limit: 10000,
          mimetype: "application/octet-stream"
        }
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader",
        options: {
          name: "local/fonts/[name].[ext]?[hash]",
          limit: 10000,
          mimetype: "image/svg+xml"
        }
      },
      {
        test: /\.gif/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader",
        options: {
          name: "local/fonts/[name].[ext]?[hash]",
          limit: 10000,
          mimetype: "image/gif"
        }
      },
      {
        test: /\.jpg/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader"
      },
      {
        test: /\.png/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader",
        options: {
          name: "local/fonts/[name].[ext]?[hash]",
          limit: 10000,
          mimetype: "image/png"
        }
      },
      {
        test: /\.mustache$/,
        loader: 'file-loader',
        options: {
          name: '[name].mustache?[hash]',
          outputPath: 'mustache/'
        }
      }
    ]
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    },
    modules: ['node_modules', 'blocks', 'local/assets/vendor'],
    extensions: ['*', '.js', '.vue']
  },
  resolveLoader: {
    modules: ['node_modules'],
    extensions: ['*', '.js']
  },
  devServer: {
    writeToDisk: true,
    contentBase: false,
    hot: true,
    port: 3100,
    open: false,
    host: "localhost",
    openPage: 'index.php',
    proxy: {
      "**/*": {
        target: HOST,
        secure: false,
        changeOrigin: true
      }
    }
  },
};
