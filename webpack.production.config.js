'use strict';

const HOST = process.env.HOST || 'http://127.0.0.1';
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const TerserPlugin = require('terser-webpack-plugin');
const entry = require('./entry');

process.env.NODE_ENV = 'production';

module.exports = {
  mode: 'production',
  entry: entry,
  output: {
    filename: 'local/[name]/[name].js',
    path: __dirname + '/local/assets/',
    publicPath: '/local/assets/',
    clean: {
      keep: /(images|vendor)/
    }
  },
  watch: false,
  watchOptions: {
    aggregateTimeout: 100,
  },
  devtool: false,
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: "local/[name]/[name].css",
      chunkFilename: "[id].css"
    }),
    new CssMinimizerPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /(node_modules|bower_components|public_html\/build\/)/,
        loader: "vue-loader"
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components|build\/)/,
        loader: "babel-loader",
        options: {
          presets: ['@babel/preset-env'],
          plugins: ['transform-object-rest-spread', 'transform-async-to-generator']
        },
      },
      {
        test: /\.sass$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: 'postcss-loader',
            options: require('./postcss.config')
          },
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              implementation: require('node-sass'),
              sassOptions: {
                indentedSyntax: true
              }
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: 'postcss-loader',
            options: require('./postcss.config')
          },
        ]
      },
      {
        test: /\.mustache$/,
        loader: 'file-loader',
        options: {
          name: '[name].mustache?[contenthash]',
          outputPath: 'mustache/'
        }
      }
    ]
  },
  resolve: {
    symlinks: false,
    alias: {
      vue: 'vue/dist/vue.min.js'
    },
    modules: ['node_modules', 'blocks', 'local/assets/vendor'],
    extensions: ['*', '.js', '.vue']
  },
  resolveLoader: {
    modules: ['node_modules'],
    extensions: ['*', '.js']
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin(),
      new CssMinimizerPlugin({
        parallel: true,
      })
    ]
  }
};
