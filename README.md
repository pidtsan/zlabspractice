# <Укажите название проекта>  

> Внимание!
> Это всего лишь произвольный шаблон документации.  
> В процессе разработки и поддержки проекта держите ее в актуальном состоянии.   
> Просто наличие файлов readme.md недостаточно.  

Основной сайт - <укажите url основного сайта>  
Тестовый сайт - <укажите url тестового сайта>  
Сайт с версткой - <укажите url сайта с версткой>  

## Как развернуть проект на локальной машине  

**Внимание. Для работы с проектом необходим web server с php 7.1. Для windows подойдет Open server.**  
**Для правильной работы системы сборки фронт-енда нужна node версии 14.15.4**

**1.** Клонировать репозиторий.

> В репозитории присутствуют символические ссылки. Поэтому для windows нужно убедиться, что включена поддержка символических
> ссылок или клонировать репозиторий командой `git clone -c core.symlinks=true <url>`

**2.** Настроить веб сервер для работы с директорией `sites/verstka` для фронт-енд части проекта и `sites/s1` для проекта  на битрикс.  
**3.** composer install (установит зависимости для php).  
**4.** npm install (если нужно работать с фронтенд частью проекта).   
**5.** Скопировать .env.example в .env и установить переменные окружения.
**6.** Развернуть резервную копию битрикс для бек-енд части проекта.  

## Frontend

### Как запустить систему сборки  

```shell script
HOST=http://<url локального сайта> npm run start
```

Сайт откроется автоматически. Теперь при изменении sass или js проект обновляется автоматически

### Как собрать проект

```shell script
npm run build
```

Команда соберет проект. Теперь можно открыть <url локального сайта> и посмотреть как сайт будет выглядеть на боевом
сервере.

#### Создание блоков при помощи командной строки
[Подробнее](https://bitbucket.org/z-labs/node-scripts/src/732aa8aeeef0c190b61040ad5b92995c7e0e30a1/docs/block.md)

### Как выполнять задачу и публиковать на боевой сервер

Фронт-енд разработчик работает в ветке frontend.

1. Обновить репозиторий `git pull`;  
2. Создать новую ветку, добавив префикс f/. Пример `git checkout -b f/first-screen-on-homepage`. Можно еще добавлять номер задачи;  
3. Пишем код, создаем атомарные и консистентные коммиты;  
4. Отправляем ветку в bitbucket `git push -u origin f/first-screen-on-homepage`;  
5. Создаем pull request, в заголовке пишем что сделали, например "Создает первый экран на главной странице", 
сообщения коммитов подтянутся списком в описание пул-реквеста;  
6. Мерджим pull request, используем стратегию squash, отмечаем галочку "close branch";  
7. Bitbucket pipelines отправит изменения на <укажите url сайта с версткой> автоматически.  

Бек-енд разработчик работает в ветке master

1. Обновить репозиторий `git pull`;  
2. Создать новую ветку, добавив префикс bx/. Пример `git checkout -b bx/first-screen-on-homepage`. Можно еще добавлять номер задачи;  
3. Пишем код, создаем атомарные и консистентные коммиты;  
4. Отправляем ветку в bitbucket `git push -u origin bx/first-screen-on-homepage`;  
5. Создаем pull request, в заголовке пишем что сделали, например "Создает первый экран на главной странице", 
сообщения коммитов подтянутся списком в описание пул-реквеста;  
6. Мерджим pull request, используем стратегию --no-ff, НЕ отмечаем галочку "close branch";  
7. Bitbucket pipelines отправит изменения на <укажите url тестового сайта> автоматически.  

#### Деплой на боевой сервер

1. Переходим в ветку production и обновляем ее;  
2. С помощью `git merge` переносим коммиты в ветку production;  
3. Тестируем;  
5. `git push`, изменения автоматически появятся на <укажите url основного сайта>;  
6. Тестируем на боевом сервере.  

## Бекенд

## Установить битрикс

При установке битрикс следует знать, что планируется хранить ядро в папке bitrix/, а upload в папке upload/ в корне 
репозитория. Символические ссылки на эти директории уже созданы.  

## Бизнес логика

> Здесь стоит описывать новые фичи проекта, если она достаточно сложная
> Это поможем быстро вспомнить как она реализована при поддержке проекта
### Формат описания фичи

<ссылка на задачу>

<Описание словами что делает эта фича>

#### Реализация

<Описание реализации с путями к файлам сайта. Необязательно описывать каждый файл, достаточно основных>
